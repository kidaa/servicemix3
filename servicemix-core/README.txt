Welcome to ServiceMix
=====================

This project makes use of the Maven tool...

http://maven.apache.org/

to build itself. Please download 1.0.2 onwards,
install it as per the instructions on Maven's site and then type

  maven

or

  maven clean test

Enjoy!
