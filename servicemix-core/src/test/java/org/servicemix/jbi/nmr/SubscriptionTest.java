/**
 * 
 * Copyright 2005 LogicBlaze, Inc. http://www.logicblaze.com
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); 
 * you may not use this file except in compliance with the License. 
 * You may obtain a copy of the License at 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, 
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 * See the License for the specific language governing permissions and 
 * limitations under the License. 
 * 
 **/
package org.servicemix.jbi.nmr;

import edu.emory.mathcs.backport.java.util.concurrent.CopyOnWriteArrayList;

import org.servicemix.MessageExchangeListener;
import org.servicemix.jbi.container.ActivationSpec;
import org.servicemix.jbi.container.JBIContainer;
import org.servicemix.jbi.container.SubscriptionSpec;
import org.servicemix.jbi.nmr.flow.FlowProvider;
import org.servicemix.tck.Receiver;
import org.servicemix.tck.ReceiverComponent;
import org.servicemix.tck.SenderComponent;

import javax.jbi.messaging.MessageExchange;
import javax.jbi.messaging.MessagingException;

import java.util.List;

import junit.framework.TestCase;

public class SubscriptionTest extends TestCase {

    public void testStNullAsync() throws Exception {
        runTest("st", null, false);
    }
    
    public void testStStAsync() throws Exception {
        runTest("st", "st", false);
    }
    
    public void testStSedaAsync() throws Exception {
        runTest("st", "seda", false);
    }
    
    public void testSedaNullAsync() throws Exception {
        runTest("seda", null, false);
    }
    
    public void testSedaStAsync() throws Exception {
        runTest("seda", "st", false);
    }
    
    public void testSedaSedaAsync() throws Exception {
        runTest("seda", "seda", false);
    }
    
    public void testStNullSync() throws Exception {
        runTest("st", null, true);
    }
    
    public void testStStSync() throws Exception {
        runTest("st", "st", true);
    }
    
    public void testStSedaSync() throws Exception {
        runTest("st", "seda", true);
    }
    
    public void testSedaNullSync() throws Exception {
        runTest("seda", null, true);
    }
    
    public void testSedaStSync() throws Exception {
        runTest("seda", "st", true);
    }
    
    public void testSedaSedaSync() throws Exception {
        runTest("seda", "seda", true);
    }
    
	private void runTest(String flowName, String subscriptionFlowName, boolean sync) throws Exception {
		JBIContainer container = new JBIContainer();
		try {
			container.getBroker().setFlow(FlowProvider.getFlow(flowName));
			if (subscriptionFlowName != null) {
				container.getBroker().getSubscriptionManager().setFlow(FlowProvider.getFlow(subscriptionFlowName));
			}
            // TODO: check why the following line is enabled, there is 
            // a 5 seconds pause when Management stuff is initialized
			//container.setCreateMBeanServer(true);
			container.init();
			container.start();
			
			SenderListener sender = new SenderListener();
            ActivationSpec senderActivationSpec = new ActivationSpec("sender", sender);
            senderActivationSpec.setFailIfNoDestinationEndpoint(false);
			container.activateComponent(senderActivationSpec);
			
			Receiver receiver1 = new ReceiverComponent();
			container.activateComponent(createReceiverAS("receiver1", receiver1));
	
			Receiver receiver2 = new ReceiverComponent();
			container.activateComponent(createReceiverAS("receiver2", receiver2));
			
			sender.sendMessages(1, sync);
			
			Thread.sleep(100);
			
			assertEquals(1, receiver1.getMessageList().getMessageCount());
			assertEquals(1, receiver2.getMessageList().getMessageCount());
			assertEquals(0, sender.responses.size());
		} finally {
			container.shutDown();
		}
	}
	
	private ActivationSpec createReceiverAS(String id, Object component) {
		ActivationSpec as = new ActivationSpec(id, component);
		SubscriptionSpec ss = new SubscriptionSpec();
		ss.setService(SenderComponent.SERVICE);
		as.setSubscriptions(new SubscriptionSpec[] { ss }); 
		return as;
	}
	
	public static class SenderListener extends SenderComponent implements MessageExchangeListener {

		public List responses = new CopyOnWriteArrayList();
		
		public void onMessageExchange(MessageExchange exchange) throws MessagingException {
			responses.add(exchange);			
		}
		
	}
	
}
