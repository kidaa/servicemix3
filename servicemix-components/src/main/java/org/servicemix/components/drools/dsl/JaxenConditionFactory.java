/** 
 * 
 * Copyright 2005 LogicBlaze, Inc. http://www.logicblaze.com
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); 
 * you may not use this file except in compliance with the License. 
 * You may obtain a copy of the License at 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, 
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 * See the License for the specific language governing permissions and 
 * limitations under the License. 
 * 
 **/
package org.servicemix.components.drools.dsl;

import org.drools.rule.Rule;
import org.drools.smf.ConditionFactory;
import org.drools.smf.Configuration;
import org.drools.smf.FactoryException;
import org.drools.spi.Condition;
import org.drools.spi.RuleBaseContext;
import org.servicemix.expression.JaxenXPathExpression;
import org.jaxen.NamespaceContext;
import org.jaxen.SimpleNamespaceContext;

/**
 * A condition which uses Jaxen based XPath expressions
 *
 * @version $Revision$
 */
public class JaxenConditionFactory implements ConditionFactory {

    public Condition[] newCondition(Rule rule, RuleBaseContext ruleBaseContext, Configuration configuration) throws FactoryException {
        String text = configuration.getText();
        if (text == null) {
            throw new FactoryException("No XPath provided!");
        }
        try {
            JaxenXPathExpression expression = new JaxenXPathExpression(text);
            expression.setNamespaceContext(createNamespaceContext(configuration));
            return new Condition[]{ new JaxenCondition(rule, expression) };
        }
        catch (Exception e) {
            throw new FactoryException(e);
        }
    }

    protected NamespaceContext createNamespaceContext(Configuration configuration) {
        SimpleNamespaceContext answer = new SimpleNamespaceContext();
        String[] names = configuration.getAttributeNames();
        for (int i = 0; i < names.length; i++) {
            String name = names[i];
            if (name.equals("xmlns")) {
                answer.addNamespace("", configuration.getAttribute(name));
            }
            else {
                if (name.startsWith("xmlns:")) {
                    String prefix = name.substring(6);
                    String uri = configuration.getAttribute(name);
                    answer.addNamespace(prefix, uri);
                }
            }
        }
        return answer;
    }
}
