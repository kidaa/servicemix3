/** 
 * 
 * Copyright 2005 LogicBlaze, Inc. http://www.logicblaze.com
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); 
 * you may not use this file except in compliance with the License. 
 * You may obtain a copy of the License at 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, 
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 * See the License for the specific language governing permissions and 
 * limitations under the License. 
 * 
 **/
package org.servicemix.components.net;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.net.SocketClient;
import org.apache.commons.net.ftp.FTPClient;
import org.servicemix.components.util.DefaultFileMarshaler;
import org.servicemix.components.util.FileMarshaler;
import org.servicemix.components.util.OutBinding;

import javax.jbi.JBIException;
import javax.jbi.messaging.MessageExchange;
import javax.jbi.messaging.MessagingException;
import javax.jbi.messaging.NormalizedMessage;
import java.io.OutputStream;
import java.io.IOException;

/**
 * A component which sends a message to a file using FTP via the
 * <a href="http://jakarta.apache.org/commons/net.html">Jakarta Commons Net</a> library
 * 
 * @version $Revision$
 */
public class FTPSender extends OutBinding {
    private static final Log log = LogFactory.getLog(FTPSender.class);

    private FTPClientPool clientPool;
    private FileMarshaler marshaler = new DefaultFileMarshaler();
    private String uniqueFileName = "ServiceMix";
    private boolean overwrite = false;


    // Properties
    //-------------------------------------------------------------------------
    public FTPClientPool getClientPool() {
        return clientPool;
    }

    public void setClientPool(FTPClientPool clientPool) {
        this.clientPool = clientPool;
    }

    public FileMarshaler getMarshaler() {
        return marshaler;
    }

    public void setMarshaler(FileMarshaler marshaler) {
        this.marshaler = marshaler;
    }

    public String getUniqueFileName() {
        return uniqueFileName;
    }

    /**
     * Sets the name used to make a unique name if no file name is available on the message.
     *
     * @param uniqueFileName the new value of the unique name to use for generating unique names
     */
    public void setUniqueFileName(String uniqueFileName) {
        this.uniqueFileName = uniqueFileName;
    }

    public boolean isOverwrite() {
        return overwrite;
    }

    public void setOverwrite(boolean overwrite) {
        this.overwrite = overwrite;
    }

    // Implementation methods
    //-------------------------------------------------------------------------
    protected void init() throws JBIException {
        if (clientPool == null) {
            throw new IllegalArgumentException("You must initialise the clientPool property");
        }
        super.init();
    }

    protected void process(MessageExchange exchange, NormalizedMessage message) throws MessagingException {
        FTPClient client = null;
        OutputStream out = null;
        try {
            client = (FTPClient) getClientPool().borrowClient();

            String name = marshaler.getOutputName(exchange, message);
            if (name == null) {
                if (uniqueFileName != null) {
                    out = client.storeUniqueFileStream(uniqueFileName);
                }
                else {
                    out = client.storeUniqueFileStream();
                }
            }
            else {
                out = client.storeFileStream(name);
                if (out == null) {
                    // lets try overwrite the previous file?
                    if (overwrite) {
                        client.deleteFile(name);
                    }
                    out = client.storeFileStream(name);
                }
            }
            if (out == null) {
                throw new MessagingException("No output stream available for output name: " + name + ". Maybe the file already exists?");
            }
            marshaler.writeMessage(exchange, message, out, name);
            done(exchange);
        }
        catch (MessagingException e) {
            throw e;
        }
        catch (Exception e) {
            throw new MessagingException(e);
        }
        finally {
            returnClient(client);
            if (out != null) {
                try {
                    out.close();
                }
                catch (IOException e) {
                    log.error("Caught exception while closing stream on error: " + e, e);
                }
            }
        }
    }

    protected void returnClient(SocketClient client) {
        if (client != null) {
            try {
                getClientPool().returnClient(client);
            }
            catch (Exception e) {
                log.error("Failed to return client to pool: " + e, e);
            }
        }
    }

}
