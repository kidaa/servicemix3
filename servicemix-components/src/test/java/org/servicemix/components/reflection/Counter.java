package org.servicemix.components.reflection;

public interface Counter {
    void increment();    
    void decrement();
}
