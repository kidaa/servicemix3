package org.servicemix.gbean;

import org.servicemix.jbi.container.JBIContainer;

public interface ServiceMixContainer {

    public JBIContainer getContainer();
}
