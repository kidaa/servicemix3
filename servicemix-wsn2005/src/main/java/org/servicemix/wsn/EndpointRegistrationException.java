package org.servicemix.wsn;

public class EndpointRegistrationException extends Exception {

	/**
	 * Generated serial version UID
	 */
	private static final long serialVersionUID = 6365080415473176527L;

	public EndpointRegistrationException() {
		super();
	}

	public EndpointRegistrationException(String message, Throwable cause) {
		super(message, cause);
	}

	public EndpointRegistrationException(String message) {
		super(message);
	}

	public EndpointRegistrationException(Throwable cause) {
		super(cause);
	}

}
