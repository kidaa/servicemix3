package org.servicemix.wsn;

import org.servicemix.wsn.jaxws.NotificationConsumer;
import org.servicemix.wsn.jaxws.PullPoint;

public interface PullPointConsumer extends PullPoint, NotificationConsumer {

}
