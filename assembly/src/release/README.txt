Welcome to ServiceMix!
======================

To find out how to get started try this
http://servicemix.org/Getting+Started

If you need more help try talking to us on our mailing lists
http://servicemix.org/Mailing+Lists


We welcome contributions
http://servicemix.org/Contributing

Many thanks for using ServiceMix.


The ServiceMix Team
http://servicemix.org/Team