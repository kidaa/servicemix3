package org.servicemix.web;
/**
 * 
 * Copyright 2005 LogicBlaze, Inc. http://www.logicblaze.com
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); 
 * you may not use this file except in compliance with the License. 
 * You may obtain a copy of the License at 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, 
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 * See the License for the specific language governing permissions and 
 * limitations under the License. 
 * 
 **/

import org.mortbay.http.SocketListener;
import org.mortbay.jetty.Server;

/**
 * A simple bootstrap class for starting Jetty in your IDE using the local web application.
 * 
 * @version $Revision$
 */
public class JettyServer {
    
    public static final int PORT = 8080;

    public static final String WEBAPP_DIR = "src/webapp";

    public static final String WEBAPP_CTX = "/";

    public static void main(String[] args) throws Exception {
        int port = PORT;
        if (args.length > 0) {
            String text = args[0];
            port = Integer.parseInt(text);
        }
        System.out.println("Starting Web Server on port: " + port);
        Server server = new Server();
        SocketListener listener = new SocketListener();
        listener.setPort(port);
        server.addWebApplication(WEBAPP_CTX, WEBAPP_DIR);
        server.addListener(listener);
        server.start();
    }
}
