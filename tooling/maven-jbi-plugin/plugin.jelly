<?xml version="1.0"?>
<!-- 
/*
 * Copyright 2001-2005 Unity Systems
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 -->
<project xmlns:j="jelly:core"
         xmlns:ant="jelly:ant"
         xmlns:license="license" 
         xmlns:maven="jelly:maven"
         xmlns:util="jelly:util"
         xmlns:artifact="artifact"
         xmlns:x="jelly:xml"
         xmlns:interaction="jelly:interaction">
	
  <!--==================================================================-->
  <!-- Default goal : Builds a jbi file                                 -->
  <!--==================================================================-->
  <goal name="jbi" prereqs="jbi:jbi" description="Build a jbi file"/>

  <!--==================================================================-->
  <!-- Initializations                                                  -->
  <!--==================================================================-->
  <goal name="jbi:init" 
    description="Initialise filesystem and other resources for a jbi">

    <ant:available property="jbiResourcesPresent" type="dir"
      file="${maven.jbi.src}"/>
      
  </goal>
  
  <!--==================================================================-->
  <!-- Builds a jbi installer file                                      -->
  <!--==================================================================-->
  <goal name="jbi:jbi" prereqs="jbi:init,jar:jar" description="Build a jbi file">

    <ant:echo>Building JBI installer ${pom.artifactId}</ant:echo>

    <ant:mkdir dir="${maven.jbi.build.dir}" />
    
    <!-- Copy resources -->
    <j:if test="${jbiResourcesPresent == 'true'}">
      <ant:copy todir="${maven.jbi.build.dir}" preservelastmodified="true">
        <ant:fileset dir="${maven.jbi.src}"
          includes="${maven.jbi.src.includes}"
          excludes="${maven.jbi.src.excludes}">
        </ant:fileset>
      </ant:copy>
    </j:if>
    <ant:copy todir="${maven.jbi.build.dir}" file="${plugin.resources}/jbi.xsd"/>
    
    <!-- Copy libs -->
    <j:forEach var="lib" items="${pom.artifacts}">
      <j:set var="dep" value="${lib.dependency}"/>     
      <j:if test="${dep.getProperty('jbi.bundle')=='true'}">
         <j:if test="${dep.type =='jar'}"> 
           <ant:copy todir="${maven.jbi.build.lib}" file="${lib.path}"/>  
         </j:if> 
      </j:if>  
    </j:forEach>  
    <ant:copy todir="${maven.jbi.build.lib}" file="${maven.build.dir}/${maven.final.name}.jar"/>        

    <ant:available property="maven.jbi.descriptor.available"
      file="${maven.jbi.descriptor}"/>
    <j:if test="${!maven.jbi.descriptor.available}">
      <attainGoal name="jbi:generate-jbi-descriptor" />
    </j:if>

    <ant:available property="maven.jbi.manifest.available"
      file="${maven.jbi.manifest}"/>

    <ant:jar 
         destfile="${maven.build.dir}/${maven.jbi.final.name}"
         basedir="${maven.jbi.build.dir}"         
         index="${maven.jbi.index}">

      <ant:metainf dir="${licenseFile.canonicalFile.parent}">
        <ant:include name="${licenseFile.canonicalFile.name}"/>
      </ant:metainf>

      <j:if test="${maven.jbi.manifest.available}">
        <ant:setProperty name="manifest" value="${maven.jbi.manifest}" />
      </j:if>

      <ant:manifest>
        <j:set var="classPath" value="${maven.jbi.classpath}"/>
        <j:if test="${!empty(classPath)}">
            <ant:attribute name="Class-Path" value="${maven.jbi.classpath}"/>
        </j:if>

        <ant:attribute name="Built-By" value="${user.name}" />
        <ant:section name="${pom.package}">
          <ant:attribute name="Specification-Title" value="${pom.artifactId}" />
          <ant:attribute name="Specification-Version"
                     value="${pom.currentVersion}" />
          <ant:attribute name="Specification-Vendor"
                     value="${pom.organization.name}" />
          <ant:attribute name="Implementation-Title"
                     value="${pom.package}" />
          <ant:attribute name="Implementation-Version"
                     value="${pom.currentVersion}" />
          <ant:attribute name="Implementation-Vendor"
                     value="${pom.organization.name}" />
        </ant:section>
      </ant:manifest>

    </ant:jar>

  </goal>

  
  
  <!--==================================================================-->
  <!-- Creates jbi descriptor - jbi.xml file                            -->
  <!--==================================================================-->
  <goal name="jbi:generate-jbi-descriptor" description="Generates the jbi descriptor">
     
     <ant:echo>Generating jbi descriptor</ant:echo>
     <ant:mkdir dir="${maven.jbi.build.dir}/META-INF" />
     <j:file  name="${maven.jbi.build.dir}/META-INF/jbi.xml"
              outputMode="xml"
              prettyPrint="true"
              encoding="${maven.jbi.descriptor.encoding}">
      <j:import file="${plugin.resources}/jbi.jsl" inherit="true" />
     </j:file>  
          
   </goal>
   

  
  <!--==================================================================-->
  <!-- Install the jbi installer in the local repository                          -->
  <!--==================================================================-->    
  <goal name="jbi:install"
        prereqs="jbi:jbi"
        description="Install the jbi installer in the local repository">
     
     <artifact:install
        artifact="${maven.build.dir}/${maven.jbi.final.name}"
        type="zip"
        project="${pom}"/> 
  
  </goal>
  
  <!--==================================================================-->
  <!-- Install the snapshot version of the jbi installer in the local repository  -->
  <!--==================================================================-->      
  <goal name="jbi:install-snapshot"
        prereqs="jbi:jbi" 
        description="Install the snapshot version of the jbi installer in the local repository">
        
      <artifact:install-snapshot
        artifact="${maven.build.dir}/${maven.jbi.final.name}"
        type="zip"
        project="${pom}"/> 
        
  </goal>

  <!--==================================================================-->
  <!-- Deploys the jbi installer to the remote repository                         -->
  <!--==================================================================-->      
  <goal name="jbi:deploy" 
        prereqs="jbi:jbi" 
        description="Deploys the jbi installer to the remote repository">

     <artifact:deploy
        artifact="${maven.build.dir}/${maven.jbi.final.name}"
        type="zip"
        project="${pom}"/>   
        
  </goal>      
        
  <!--==================================================================-->
  <!-- Deploys the snapshot version of the jbi installer to the remote repository         -->
  <!--==================================================================-->      
  <goal name="jbi:deploy-snapshot" 
        prereqs="jbi:jbi"
        description="Deploys the snapshot version of the jbi to remote repository">  

     <artifact:deploy-snapshot
        artifact="${maven.build.dir}/${maven.jbi.final.name}"
        type="zip"
        project="${pom}"/>    
        
  </goal>

	
	
	
	
  <goal name="jbi:generateInstaller" prereqs="jar:jar" description="Generates a JBI installer">  		  	  
    <ant:mkdir dir="${jbi.build.dir}/lib"/>	
    <ant:mkdir dir="${jbi.build.dir}/META-INF"/>		
    <j:file name="${jbi.build.dir}/META-INF/jbi.xml" prettyPrint="true"
            outputMode="xml" escapeText="false">
      <j:import file="${jbi.xml.template}" inherit="true" />
    </j:file>
	
    <util:file var="jbiDirectory" name="${jbi.base.directory}"/>
      <j:if test="${servicesFile.exists()}">
        <copy todir="${jbi.build.dir}">
          <fileset dir="${jbi.base.directory}"/>
        </copy>
    </j:if>
    <ant:copy file="${plugin.resources}/jbi.xsd" todir="${jbi.build.dir}"/>
    <ant:copy file="${plugin.resources}/service.xsd" todir="${jbi.build.dir}"/>	
    <ant:zip destfile="${maven.build.dir}/${maven.final.name}-jbi-installer.zip" basedir="${jbi.build.dir}">
      <include name="**/*"/>            	
    </ant:zip>
  </goal>
  
  <goal name="jbi:createArchetype" description="Generates a template JBI project">  		  	  
    <interaction:ask question="Enter the name for your new JBI project" prompt="?" answer="projectName"/>
    <ant:mkdir dir="${projectName}/src/main/java/"/>
    <ant:mkdir dir="${projectName}/src/main/test/"/>  	
    <ant:mkdir dir="${projectName}/src/main/merge/"/>  	  	
    <ant:mkdir dir="${projectName}/src/main/jbi/META-INF"/>  	  	  	  	
    <j:file name="${projectName}/project.xml" prettyPrint="true"
            outputMode="xml" escapeText="false">
      <j:import file="${plugin.resources}/project.jsl" inherit="true" />
    </j:file>
    <ant:copy file="${plugin.resources}/project.properties" todir="${projectName}"/>
    <ant:copy file="${plugin.resources}/jbi-spring.xml" todir="${projectName}/src/main/jbi/META-INF"/>
    <ant:copy file="${plugin.resources}/services.xml" todir="${projectName}/src/main/merge"/>	
  </goal>
</project>
